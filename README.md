# Worker application - Accenture Assignment

This microservice is responsible for retrieving, creating and updating orders made by the frontend client. The application also communicates with the Payment application through an HTTP endpoint to retrieve the 'payment' result generated from it and updates the database accordingly.

The solution is deployed on AWS Elastic Beanstalk in the NodeJS environment.

## Stack
- NodeJS + AWS SDK
- AWS Elastic Beanstalk

## Running the application

1. Clone or download the repository, navigate into it and run the npm install command
```
git clone
cd accenture-worker
npm install
```
2. To start the application, run the following command
```
npm start
```
