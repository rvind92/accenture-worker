const { Consumer } = require('sqs-consumer')
const axios = require('axios')
const AWS = require('aws-sdk')
const URL = process.env.URL || 'http://localhost:3000'
const QUEUE_URL = 'https://sqs.us-east-2.amazonaws.com/723249754734/accenture-worker-queue'
const states = [ true, false ]

AWS.config.update({
  region: 'us-east-2',
  accessKeyId: 'AKIA2QZILOJXO36BDGN2',
  secretAccessKey: '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'
})

const sqs = new AWS.SQS({ apiVersion: '2012-11-05' })

const app = Consumer.create({
  queueUrl: QUEUE_URL,
  waitTimeSeconds: 0,
  handleMessage: async (message) => {
    const { Body, ReceiptHandle } = message
    const payment = await completeOrder(Body)
    response(payment)

    const deleteParams = {
      QueueUrl: QUEUE_URL,
      ReceiptHandle
    }

    const deleteMessage = sqs.deleteMessage(deleteParams).promise()
    deleteMessage
      .then(data => console.log("Message Deleted", data))
      .catch(err => console.log("Delete Error", err))
  },
  sqs: new AWS.SQS()
})

const completeOrder = (data) => {
  const { booking } = JSON.parse(data)
  const state = states[Math.floor(Math.random() * states.length)]
  booking.status = state
  const delay = Math.random() * Math.floor(8) * 1000
  return booking
}

const response = async message => {
  try {
    await axios.post(`${URL}/api/payment`, message)
  } catch (e) {
    console.log({
      message: 'Unable to reach the server',
      error: e
    })
  }
}

app.on('error', err => {
  console.error(err.message)
})

app.on('processing_error', err => {
  console.error(err.message)
})

app.on('timeout_error', err => {
  console.error(err.message)
})

app.start()
